from statistics import mode
from django.db import models

# Create your models here.


class WebsiteCategory(models.Model):
    name = models.CharField(max_length=10)
    description = models.CharField(max_length=50)
    date_added = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    count = models.PositiveIntegerField(default=0)

    def __str__(self):
        return f"{self.name}"


class Website(models.Model):
    url = models.URLField()
    title = models.CharField(max_length=10)
    meta_description = models.CharField(max_length=50)
    alexa_rank = models.PositiveIntegerField(default=0)
    category = models.ForeignKey(WebsiteCategory, on_delete=models.CASCADE)
    date_added = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.title}"


class WebPage(models.Model):
    website = models.ForeignKey(Website, on_delete=models.CASCADE)
    url = models.URLField(unique=True)
    date_added = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=10)
    meta_description = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.url}"
