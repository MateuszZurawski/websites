from django.urls import path

from . import views

urlpatterns = [
    path('', views.WebsiteListView.as_view(), name='index'),
    path('details/<website_id>', views.detailedView, name='index')
]
