from django.views.generic import ListView
from django.shortcuts import render

from.models import Website, WebsiteCategory
# Create your views here.


class WebsiteListView(ListView):
    paginate_by = 25
    model = Website
    template_name = "website/index.html"

    def get_queryset(self):
        if self.request.GET.get("category", None):
            return Website.objects.filter(category__name=self.request.GET["category"])
        return Website.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["categories"] = WebsiteCategory.objects.all()
        context["website_fields"] = ("url", "title", "alexa_rank")
        return context

    def get_ordering(self):
        ordering = self.request.GET.get("ordering", "-date_added")
        return ordering


def detailedView(request, website_id):
    website = Website.objects.get(id=website_id)
    context = {"website_data": website}
    return render(request, "website/detailed.html", context)
