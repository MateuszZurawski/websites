from django.contrib import admin
from .models import WebPage, Website, WebsiteCategory
# Register your models here.
admin.site.register(WebPage)
admin.site.register(Website)
admin.site.register(WebsiteCategory)
